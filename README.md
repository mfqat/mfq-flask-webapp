# mfq-flask-app

## Generar una nueva versión de la imagen
```
docker build -t maurofq/mfq-flask-app:v2 ./app
```
## Creamos secreto de kubernetes que almacenará las credenciales de docker hub:
```
kubectl create secret generic regcred --from-file=.dockerconfigjson=/home/mfqat91/.docker/config.json --type=kubernetes.io/dockerconfig.json
```

## Subir la imagen a docker hub
```
docker image push maurofq/mfq-flask-app:v1
```

## Una vez subida la imagen, podemos generar un deployment que descargue esa imagen y exponerlo:
```
kubectl create -f .\k8s-flask-webapp.yaml
```

## Para destruir todo lo que acabos de crear:
```
kubectl delete -f .\k8s-flask-webapp.yaml
```