from flask import Flask, request
import socket
app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<h1> Hello from mfq-app-flask!!'

@app.route('/remote-ip')
def client():
    ip_addr = request.environ['REMOTE_ADDR']
    return '<h1> Requester IP address is:' + ip_addr

@app.route('/hostname')
def return_hostname():
    return "Pod --> {}".format(socket.gethostname())

if __name__ == '__main__':
    app.run(debug=True)
